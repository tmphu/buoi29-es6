import { getToDoList, deleteToDo, putToDo } from "./service.js";

export function getFormData() {
  return document.getElementById("newTask").value;
}

function renderToDoList(list) {
  let contentToDo = "",
    contentComplete = "";
  list.forEach((item) => {
    // render uncompleted tasks
    if (!item.isComplete) {
      contentToDo += `
        <li>
            <div>${item.desc}</div>
            <div class="buttons">
                <button class="remove" onClick="deleteToDoItem(${item.id})"><i class="fa fa-trash-alt"></i></button>
                <button class="mark" onClick="markItemComplete(${item.id})"><i class="fa fa-check-circle"></i></button>
            </div>
        </li>
        `;
    }
    // render completed tasks
    else {
      contentComplete += `
        <li>
            <div class="desc done">${item.desc}</div>
            <div class="buttons">
                <button class="remove done" onClick="deleteToDoItem(${item.id})"><i class="fa fa-trash-alt"></i></button>
                <button class="mark done" onClick="markItemUncomplete(${item.id})"><i class="fa fa-check-circle"></i></button>
            </div>
        </li>
        `;
    }
  });
  document.getElementById("todo").innerHTML = contentToDo;
  document.getElementById("completed").innerHTML = contentComplete;
}

export function fetchToDoList() {
  return getToDoList()
    .then((res) => {
      renderToDoList(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
}

export function deleteToDoItem(id) {
  return deleteToDo(id).then((res) => {
    fetchToDoList();
  });
}
window.deleteToDoItem = deleteToDoItem;

export function markItemComplete(id) {
  let payload = { isComplete: true };
  return putToDo(id, payload)
    .then((res) => {
      fetchToDoList();
    })
    .catch((err) => {
      console.log(err);
    });
}
window.markItemComplete = markItemComplete;

export function markItemUncomplete(id) {
  let payload = { isComplete: false };
  return putToDo(id, payload)
    .then((res) => {
      fetchToDoList();
    })
    .catch((err) => {
      console.log(err);
    });
}
window.markItemUncomplete = markItemUncomplete;

function compareAZ(a, b) {
  const descA = a.desc.toUpperCase();
  const descB = b.desc.toUpperCase();

  if (descA < descB) {
    return -1;
  }
  if (descA > descB) {
    return 1;
  }
  return 0;
}

export function sortAZ() {
  getToDoList()
    .then((res) => {
      const list = res.data;
      list.sort(compareAZ);
      renderToDoList(list);
    })
    .catch((err) => {
      console.log(err);
    });
}
window.sortAZ = sortAZ;

function compareZA(a, b) {
  const descA = a.desc.toUpperCase();
  const descB = b.desc.toUpperCase();

  if (descA > descB) {
    return -1;
  }
  if (descA < descB) {
    return 1;
  }
  return 0;
}

export function sortZA() {
  getToDoList()
    .then((res) => {
      const list = res.data;
      list.sort(compareZA);
      renderToDoList(list);
    })
    .catch((err) => {
      console.log(err);
    });
}
window.sortZA = sortZA;
