import { postToTo, deleteToDo } from "./service.js";
import { getFormData, fetchToDoList } from "./controller.js";
import { ToDo } from "./model.js";

fetchToDoList();

async function addToDo() {
  let desc = getFormData();
  let toDoItem = new ToDo(desc, false);
  await postToTo(toDoItem);
  fetchToDoList();
  document.getElementById("newTask").value = "";
}
window.addToDo = addToDo;
