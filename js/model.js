export class ToDo {
  constructor(desc, isComplete) {
    this.desc = desc;
    this.isComplete = isComplete;
  }
}
