let BASE_URL = "https://636150a5af66cc87dc28e460.mockapi.io";

export function getToDoList() {
  return axios({
    url: `${BASE_URL}/todos-buoi29`,
    method: "GET",
  });
}

export function postToTo(payload) {
  return axios({
    url: `${BASE_URL}/todos-buoi29`,
    method: "POST",
    data: payload,
  });
}

export function deleteToDo(id) {
  return axios({
    url: `${BASE_URL}/todos-buoi29/${id}`,
    method: "DELETE",
  });
}

export function putToDo(id, payload) {
  return axios({
    url: `${BASE_URL}/todos-buoi29/${id}`,
    method: "PUT",
    data: payload,
  });
}
